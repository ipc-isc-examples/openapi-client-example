package org.school.isc.openapiclientexample;

import org.school.isc.api.PetsControllerApi;
import org.school.isc.model.Pet;
import org.school.isc.model.Pets;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenapiClientExampleApplication {

    private final static PetsControllerApi api = new PetsControllerApi();

    public static void main(String[] args) {
        SpringApplication.run(OpenapiClientExampleApplication.class, args);
        final Pets pets = api.listPets(3);
        for (final Pet pet : pets) {
            System.out.println(pet.getTag());
            System.out.println(pet.getName());
            System.out.println(pet.getId());
        }
    }

}
